@extends('site.app')
@section('content')
    <div class="page single-page">
        @yield('single-page')
    </div>
@endsection