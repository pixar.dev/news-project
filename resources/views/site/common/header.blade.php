<div class="header">
    @include('site.common.sub.header.header-left')
    @include('site.common.sub.header.social-icons')
    <div class="clearfix"></div>
    @include('site.common.sub.header.header-right')
    <div class="clearfix"></div>
</div>