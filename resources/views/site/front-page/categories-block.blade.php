<div class="section section-post-categories">
    <div class="posts-grid-left">
        @include('site.front-page.sub._chinh-tri')
        <!--- Include !-->
        @include('site.front-page.sub._quoc-te')
        <!--- Include !-->
        @include('site.front-page.sub._kinh-te')
        <!--- Include !-->
        @include('site.front-page.sub._giao-duc')

    </div>
    <div class="posts-grid-right">
        <!--- Include !-->
        @include('site.front-page.sub._phap-luat')
        <!--- Include !-->
        @include('site.front-page.sub._y-te')
        <!--- Include !-->
        @include('site.front-page.sub._van-hoa')
        <!--- Include !-->
        @include('site.front-page.sub._the-thao')
        <!--- Include !-->
        @include('site.front-page.sub._adv-bottom')
    </div>
    <div class="clearfix"></div>
</div>
