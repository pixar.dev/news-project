@extends('site.app')
@section('content')
    <div class="page page-fontpage">
        @include('site.front-page.index-block')
        @include('site.front-page.categories-block')
    </div>
@endsection